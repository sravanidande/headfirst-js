// import { hello } from './hello';

// export class Point {
//   x = 1;
//   y = 2;
// }

// console.log(hello(), ...[1, 2, 3], new Point());


function quack(num){
  for(var i=0;i<num;i++){
    console.log("Quack!")
  }
}

quack(3);

var fly = function(num){
   for(var i=0;i<num;i++){
    console.log("Flying!")
  }
}

fly(3);


var migrating = true;


var fly = function(num){
  for(var i=0;i<num;i++){
   console.log("Flying!")
 }
}

function quack(num){
  for(var i=0;i<num;i++){
    console.log("Quack!")
  }
}

if(migrating){
  quack(4);
  fly(4);
}


var superFly = fly;
superFly(2);

var superQuack = quack;
superQuack(3);

var passengers = [
  {name:"jane" ,paid:true,ticket:"coach"},
  {name:"evel",paid:true,ticket:"firstclass"},
  {name:"sue",paid:false,ticket:"firstclass"},
  {name:"Funall",paid:true,ticket:"coach"}
]


function passengersPaid (passengers){
  var people = []
  for(var i=0;i<passengers.length;i++){
    if(passengers[i].paid){
    people.push(passengers[i].name)
    }
  }
  return people
}

console.log(passengersPaid(passengers));


function allPassengers(passengers){
  var result = []
  for(var i=0;i<passengers.length;i++){
    result.push(passengers[i].name)
  }
  return result;
}

console.log(allPassengers(passengers));


function processPassengers(passengers,testFunction){
  for(var i=0;i<passengers.length;i++){
    if(testFunction(passengers[i])){
      return true
    }
  }
  false
}


function checkNoFlyList(passenger){
  return (passenger.name === "joe")
}

// console.log(checkNoFlyList({name:"evel",paid:"true"}))

function checkNoPaid(passenger){
  return (passenger.paid === false)
}
// console.log(checkNoPaid({name:"leo",paid:false}))

var allCanFly = processPassengers(passengers,checkNoFlyList)
  if(!allCanFly){
    console.log("we can takeoff")
  }else{
    console.log("we can not")
  }


// console.log(allCanFly(passengers,checkNoFlyList));


var allPaid = processPassengers(passengers,checkNoPaid)
  
if(!allPaid){
  console.log("we can takeoff")
}else{
  console.log("we can not")
}


// console.log(allCanFly(passengers,checkNoPaid));


function passengerService(passengers){
  for(var i=0;i<passengers.length;i++){
    if(passengers[i].ticket==="firstclass"){
      console.log("order wine")
    }
  }
  console.log("order water r cola")
}

console.log(passengerService(passengers));

function createDrinkOrder(passenger){
  var orderFunction;
  if(passenger.ticket==="firstclass"){
    orderFunction = function(){
      console.log("would you like a cocktail or wine?");
    }
  }else{
      orderFunction = function(){
        console.log("your choice is cola or water")
      }
    }
  
  return orderFunction
}

console.log(createDrinkOrder({name:"joe",ticket:"coach"}))

function serveCustomer(passenger){
var getDrinkOrder = createDrinkOrder(passenger)
getDrinkOrder();
}

console.log(serveCustomer({name:"joe",ticket:"firstclass"}))

function servePassenger(passengers){
  for(var i=0;i<passengers.length;i++){
    // console.log(passengers[i])
    serveCustomer(passengers[i])
    // console.log(passengers[i])
  }
  return false
}

console.log(servePassenger(passengers))


function compareNumbers(num1,num2){
  if(num1>num2){
    return 1
  }else if(num1 === num2){
    return 0
  }else{
    return -1
  }
}

console.log(compareNumbers(21,30))


var arrayNumbers = [50,66,22,30,45]

console.log(arrayNumbers.sort(compareNumbers));

function compareNumbersDesc(num1,num2){
  if(num1<num2){
    return 1
  }else if(num1 === num2){
    return 0
  }else{
    return -1
  }
}

console.log(arrayNumbers.sort(compareNumbersDesc))

var products = [
  {name:"grape",calories:170,color:"red",sold:8200},
  {name:"orange",calories:160,color:"orange",sold:12101},
  {name:"cola",calories:210,color:"caramel",sold:25412},
  {name:"diet cola",calories:0,color:"caramel",sold:43922},
  {name:"lemon",calories:200,color:"clear",sold:14983},
  {name:"raspberry",calories:180,color:"pink",sold:9427},
  {name:"root beer",calories:200,color:"caramel",sold:9909},
  {name:"water",calories:0,color:"clear",sold:62123},
]

function comapreSold(colaA,colaB){
  if(colaA.sold>colaB.sold){
    return 1
  }else if( colaA.sold===colaB.sold){
    return 0
  }
  else{
    return -1
  }
}

console.log(products.sort(comapreSold))

function compareColor(colaA,colaB){
  if(colaA.color > colaB.color){
    return 1
  }
  else if(colaA.color === colaB.color){
    return 0
  }
  else{
    return -1
  }
}

console.log(products.sort(compareColor));


function createDinnerOrder(passenger){
  var orderFunction;
  if(passenger.ticket === "firstclass"){
    orderFunction = function(){
      console.log("order can be chicken and wine")
    }
  }else{
    orderFunction = function(){
      console.log("allowed to order pizza or burger")
    }
  }
  return orderFunction;
}

function serveCustomers(passenger){
var getDinnerOrder = createDinnerOrder(passenger)
getDinnerOrder();
}

function servePassengers(passengers){
  for(var i=0;i<passengers.length;i++){
    serveCustomers(passengers[i])
  }
}

console.log(servePassengers(passengers));


function paidPassengers(passengers){
  for(var i=0;i<passengers.length;i++){
    if(passengers[i].paid){
      console.log(passengers[i].name+" has paid")
    }else{
      console.log(passengers[i].name+" has not paid")
    }
  }
}

console.log(paidPassengers(passengers))

// 11th chapter//

function cookieAlarm(){
  console.log("cookie baked")
}

console.log(setTimeout(cookieAlarm,5000))


setTimeout(function(){console.log("thank you for waiting it is baked")},5000)


var migrating = true

if(migrating){
  quack1(3)
  // fly1(4)
}

var fly1 = function(num){
  for(var i=0;i<num;i++){
    console.log("flying flying")
  }
}


function quack1(num){
  for(var i=0;i<num;i++){
    console.log("quack quack")
  }
}

var migrating = true

var fly2 = function(num){
  var sound = "Flying"
  function wrapper(){
    console.log(sound)
  }
  for(var i=0;i<num;i++){
    wrapper()
  }
}


function quack2(num){
  var sound= "Quacker"
  var quacker = function(){
    console.log(sound)
  }
  for(var i=0;i<num;i++){
    quacker()
  }
}

if(migrating){
  quack2(3);
  fly2(4)
}

var justAvar = "iam global"

function whereAreYou(){
  var justAvar = "iam local"
  function inside(){
     return justAvar
  }
  return inside
}

var innerFunction = whereAreYou()
var result = innerFunction()
console.log(result)
// console.log(whereAreYou())

// var count = 0

// function counter(){
//   count = count+1
//   return count
// }

// console.log(counter())
// console.log(counter())
// console.log(counter())


function makeCounter(){
  var count= 0
  function counter(){
    count = count+1
    return count
  }
  return counter
}

var doCount = makeCounter()
console.log(doCount())
console.log(doCount())
console.log(doCount())
// var result = doCount()
// console.log(result)

// var count= 0

window.onload=function(){
  var button =  document.getElementById("clickme")
  button.onclick = clickhandler
}

function clickhandler(){
  var message= "you clicked me"
  var div=document.getElementById("message")
  count++
  div.innerHTML = message+count+"times!"
}


window.onload = function(){
  var count= 0
  var message = "you clicked me"
  var div = document.getElementById("message")

  var button = document.getElementById("clickme")
  button.onclick=function(){
    count++
    div.innerHTML = message+count + " times!"
  }
}